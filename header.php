<!--A Design by GrootCode Pvt.Ltd.
Author: GrootCode
Author URL: http://grootcode.com
-->
<!DOCTYPE HTML>
<html>
<head>
<title>GOVT. INDUSTRIAL TRAINING INSTITUTE</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Learn Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />

<link href="css/bootstrap-theme.min.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bootstrap.newsbox.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="css/jquery.countdown.css" />
<link rel="stylesheet" href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<link rel="stylesheet" href="css/bootstrap-image-gallery.min.css">

<link href='//fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
<!----font-Awesome----->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!----font-Awesome----->
<script>
$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
            $(this).toggleClass('open');       
        }
    );
});
</script>
<script type="text/javascript">
$(function () {
$(".demo").bootstrapNews({
newsPerPage: 4,
navigation: true,
autoplay: true,
direction:'up', // up or down
animationSpeed: 'normal',
newsTickerInterval: 4000, //4 secs
pauseOnHover: true,
onStop: null,
onPause: null,
onReset: null,
onPrev: null,
onNext: null,
onToDo: null
});
});
</script>
<style>
    .navbar-brand {
  padding: 0px;
}
.navbar-brand>img {
/*  height: 50%;*/
  padding: 0px;
/*  width: 40%;*/
}
</style>
</head>
<body>
<div style="box-shadow: 0 0 10px black;">
<nav class="navbar navbar-default" role="navigation">
	<div class="container">
	    <div class="navbar-header">
	        
	       
           <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="Dispute Bills">
        </a>
	        
	    </div>
	    
        <h2 style="font-family: Verdana; font-weight: normal; color: #f5f5f5; text-align:center; margin-top:36px"> GOVT. INDUSTRIAL TRAINING INSTITUTE <br>BHARMOUR, HIMACHAL PRADESH  </h2>
        <h4 style="font-family: Verdana; font-weight: normal; color: #f5f5f5; text-align:center"> AFFILIATION: HIMACHAL PRADESH TAKNIKI <br> SHIKSHA BOARD DHARAMSHALA</h4>
	    <!--/.navbar-header-->

	   
	    

	    <div class="clearfix"> </div>
	  </div>
	    <!--/.navbar-collapse-->
</nav>
<nav class="navbar nav_bottom" role="navigation">
 <div class="container">
 <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header nav_2">
      <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"></a>
   </div> 
   <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
        <ul class="nav navbar-nav nav_1">
            <li><a href="index.php">Home</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Instition Profile<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="about.php">About Institution</a></li>
               
                <li><a href="faq.php">Vision</a></li>
              </ul>
            </li>
            
			<li><a href="faculty.php">Faculty</a></li>
           <li><a href="fee.php">Admission Fee</a></li>
            <li><a href="hostel.php">Hostel</a></li>
            <li><a href="galary.php">Photo Gallery</a></li>
            <li><a href="alumini.php">Alumini</a></li>
             <li><a href="shortcodes.php">Principal Desk's</a></li>
            <li><a href="career.php">Media</a></li>
            <li class="last"><a href="contact.php">Contacts</a></li>
        </ul>
     </div><!-- /.navbar-collapse -->
   </div>
</nav>
   <link rel="stylesheet" href="css2/normalize.css">

    
        <link rel="stylesheet" href="css2/style.css">