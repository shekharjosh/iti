<!--Header Start--> 
     <?php 
      // this calls the common header for all the menu pages.
      include_once('header.php'); 
     ?>
      <!--Header End--> 
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Principal's Desk</h3>
  		
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="index.html">Home</a></li>
                <li class="current-page">Principal's Desk</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
    <div style="height: 200px;">
<center><img src="images/fc.jpg" style="margin: 0 auto;height:200px;width:200px;"></center>
</div>
<div style=" height:100%; width:80%; margin:0% 10% 5% 10%;" >
<p>
    Economic growth for spring of a nation’s overall development requires not only physical resources and technical facilities but also the human resources to exploit the technologies properly. For overall economic growth and speedy development, “Human Capital” or “Human Infrastructure” is important as any other type of infrastructure. Technology and skill which play a prominent role in productive processes need properly qualified and trained manpower. Qualified and competent in Industry and other projects are equally important.<br><br>

There are various Institutions to educate and train the technocrats. Whereas the higher education is imparted by the Engineering Colleges and Polytechnics, the Industrial Training Institutes i.e. I.T.I’s play a vital role to produce the middle level trained manpower required for wide range of productive activities in various Industries. In the middle cadre of trained, “Craftsmanship” plays a pivotal role. The term, “CRAFTMAN” refers to a heterogeneous group of production process workers and includes a variety of trade skills, occupations, education and training backgrounds. The Industrial Training Institute (I.T.I’s) conduct recognized trade courses and thus, generates craftsman of varied skills on a continuous basis. The main aim and objective of CTS (Craftsman Training Scheme) is to impart vocational training through Industrial Training Institutes in the country. Industrial Training Institutes are one of the major institutional sources for the supply of craftsman in the country.<br><br>

Job oriented structured and entrepreneurship training is very essential in order to have job opportunities and scope for self-employment for the young generation. Industrial Training Institutes play an important role to achieve this goal. Hence, it becomes essential to open the new Institutions according to demand and need of the area for providing vocational training to the youth so that they may be able to get employment in the industries or may be able to set up their units for generating employment for unskilled masses.<br><br>

Over the years, a number of weaknesses developed in the vocational training system in the country. The major deficiencies are rigid training structure, inadequate vocational instructors, lack of modern equipment and machinery and weak linkage with the industry. These deficiencies created a mismatch between the trained manpower produced by the ITIs and those required by the industry. Therefore, a strong need was felt to upgrade the vocational training infrastructure in the country through PPP mode. <br><br>

The ultimate objective of any technology is to ensure that it provides comfort, leisure, productivity and improve the quality of life of citizens. As global economic competition grows sharper, technical & vocational education becomes an important source of competitive advantage, closely linked to economic growth, and a way for countries to attract jobs and investment. Countries like India therefore frequently see raising technical & vocational educational attainment as a way to raise the Human Capital Index. <br><br>

The 21st century education is bold. It breaks the mold. It is flexible, creative, challenging, and complex. It is project based. The curriculum is inter- disciplinary, project based, and research-driven. It is connected to the community-local, state, national, and global. The curriculum incorporate higher order thinking skills, multiple intelligences, technology and multimedia, the multiple literalizes of the 21st century, and authentic assessments. Service learning is an important component. In 21 century, “LEARNING” is a key word for success of Individual as well as Organization. “Learning” is more relevant in Indian Context because most of the time copying from outside system provide only a short term solution for any coming problem. Learning facilitates a better platform at which a range of Innovation can flourish in all types of sub systems.
<br><br>
Due to fast technological changes, regular skills upgradation and multi skilling is indeed very essential to ensure the employability of labour force. Further, every year 6 to 8 million new persons are added to the labour force. They also require skill development. But the present facilities for skill development are highly inadequate in the country.<br><br>

For the Industrial growth in the country and for Industry to meet the challenges of new Technology, there is need for close co-operation and harmonious relations between Institute and Industry. The need of the Industry-Institute interaction Programme is to raise the skills of the students to match the requirements of Industry and to fill the gap between the trainees and the need of modern Industry. This gap can be filled up by the PPP mode of Education System.<br><br>

Skills and knowledge are the driving forces of economic growth and social development of any country. The economy becomes more productive, innovative and competitive through the existence of more skilled human potential. The level of employment, its composition and the growth in employment opportunities is a critical indicator of the process of development in any economy.<br><br>

Increase in quality of work has direct relation with productivity, which in turn is connected with skill availability of the workforce. In this context, it is necessary not only to create quality employment but also to equip such quality employment with sufficient skills. Creating quality employment and equipping the labour force with sufficient skills are thus the major challenges before all the states in the country.
    
</p>


</div>


	
   
   
   
    <!--Footer Start--> 
     <?php 
      // this calls the common footer for all the menu pages.
      include_once('footer.php'); 
     ?>
      <!--footer End--> 