<!--Header Start--> 
     <?php 
      // this calls the common header for all the menu pages.
      include_once('header.php'); 
     ?>
      <!--Header End--> 
      
      <!--New Stylesheet for Alumini start-->
      
      <link rel="stylesheet" href="css2/normalize.css">

    
        <link rel="stylesheet" href="css2/style.css">
      
      
       <!--New Stylesheet for Alumini end-->
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Alumini</h3>
  		
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="index.html">Home</a></li>
                <li class="current-page">Alumini</li>
            </ul>
        </div>
  	</div>
  </div>
   <?php 
$json = file_get_contents('data.json');
$data = json_decode($json,true);
?>
    <!-- //banner -->
	<div class="features">
	   <div class="container">
	   	  <h2>LIST OF ALUMINI 2015-16</h2>

            <table class="responstable">
  
          <tr>
            <th>Serial No.</th>
            <th> Student Name</th>
            <th>Father Name</th>
            <th>Trade</th>
            <th>Contact No.</th>
          </tr>
  
         
  <?php 
                $init = 1;
             foreach($data as $d){
                 echo '<tr>';
                 
                  echo "<td>$init</td>";
                 echo "<td>".$d['Student Name']."</td>";
                  echo "<td>".$d['Father Name']."</td>";
                echo "<td>".$d['Trade']."</td>";
                echo "<td>".$d['Mobil No.']."</td>";
                 
                 
                 echo '</tr>';
                 $init++;
                 
             }   
                
                ?>
         
  
        </table>
	   	   <script src='js/respond.js'></script>
	  </div>
	</div>
	
	<!--Footer Start--> 
     <?php 
      // this calls the common footer for all the menu pages.
      include_once('footer.php'); 
     ?>
      <!--footer End--> 