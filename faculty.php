<!--Header Start--> 
     <?php 
      // this calls the common header for all the menu pages.
      include_once('header.php'); 
     ?>
      <!--Header End--> 
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Faculty</h3>
  		
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="index.html">Home</a></li>
                <li class="current-page">Faculty</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="admission">
	   <div class="container">
	   	 <div class="faculty_top">
	   	 
	   	  <div class="bottom_content">  
   	 <h3>Our Faculty</h3>
     <div class="grid_2">
     	<div class="col-md-4 portfolio-left">
            <div class="portfolio-img event-img">
                <img src="images/fc1.jpg" class="img-responsive" alt=""/>
                <div class="over-image"></div>
            </div>
            <div class="portfolio-description">
               <h4><a href="#">Joginder Kumar</a></h4>
               <p> <b>Designation:</b> Instructor Plumber
                <b>Qualifications:</b>Matric, NTC in Plumber trade<br>
                <b>Total Experience:</b>20 Years Industrial and teaching xperience.</p>
                
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="col-md-4 portfolio-left">
            <div class="portfolio-img event-img">
                <img src="images/fc2.jpg" class="img-responsive" alt="" height/>
                 <div class="over-image"></div>
            </div>
            <div class="portfolio-description">
               <h4><a href="#">	Krishan Kumar</a></h4>
              <p> <b>Designation:</b> Instructor Plumber
                <b>Qualifications:</b>BA, B.ed, PGDCA, MSc<br>
                <b>Total Experience:</b>6 Years teaching experience.</p>
               
                
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="col-md-4 portfolio-left">
            <div class="portfolio-img event-img">
                <img src="images/fc3.jpg" class="img-responsive" alt=""/>
                 <div class="over-image"></div>
            </div>
            <div class="portfolio-description">
               <h4><a href="#">Kiran Devi</a></h4>
               <p> <b>Instructor:</b> Trainer, Dress Making.
                <b>Qualifications:</b>BA, ITI Dress Making.<br>
                <b>Total Experience:</b>5.6 Years work in Teaching and private firm.</p>
               
            </div>
            
        </div>
        
     </div>
    
    </div>
 
	   	 
<!--
	   	  <div class="col-md-4 faculty_grid">
	   	  	<figure class="team_member">
	   	  		<img src="images/fc.jpg" class="img-responsive wp-post-image" alt=""/>
	   	  		<div></div>
	   	  		<figcaption><h3 class="person-title"><a href="event_single.html">Anil Kumar</a></h3>
	   	  			<span class="person-deg">Principal</span>
	   	  			<span class="person-deg">Graduation: B_Tech In Electronics & Communications</span>
	   	  			<span class="person-deg">Post Graduation diploma: Industrial Safety, Environment Management, Personal Management & Labour Welfare, Industrial Safety Management</span>
	   	  			
	   	  			<p><a href="bmr.iti@gmail.com">bmr.iti@gmail.com</a></p>
	   	  			<p>Total Experience: 6 Years</p>

	   	  			<div class="person-social">
	   	  				<ul class="social-person">
	   	  					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-youtube"></i></a></li>
	   	  			    </ul>
	   	  		  </div>
	   	  		  
	   	  	   </figcaption>
	   	  	 </figure>
	   	  </div>
-->
<!--
	   	  <div class="col-md-4 faculty_grid">
	   	  	<figure class="team_member">
	   	  		<img src="images/fc1.jpg" class="img-responsive wp-post-image" alt=""/>
	   	  		<div></div>
	   	  		<figcaption><h3 class="person-title"><a href="event_single.html">Readable Content </a></h3>
	   	  			<span class="person-deg">Pg, Degree</span>
	   	  			<p><a href="mailto@example.com">info(at)Learn.com</a></p>
	   	  			<p>4 Years</p>
	   	  			<div class="person-social">
	   	  				<ul class="social-person">
	   	  					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-youtube"></i></a></li>
	   	  			    </ul>
	   	  		  </div>
	   	  	   </figcaption>
	   	  	 </figure>
	   	  </div>
-->
<!--
	   	  <div class="col-md-4 faculty_grid">
	   	  	<figure class="team_member">
	   	  		<img src="images/fc2.jpg" class="img-responsive wp-post-image" alt=""/>
	   	  		<div></div>
	   	  		<figcaption><h3 class="person-title"><a href="event_single.html">Readable Content </a></h3>
	   	  			<span class="person-deg">Pg, Degree</span>
	   	  			<p><a href="mailto@example.com">info(at)Learn.com</a></p>
	   	  			<p>4 Years</p>
	   	  			<div class="person-social">
	   	  				<ul class="social-person">
	   	  					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-youtube"></i></a></li>
	   	  			    </ul>
	   	  		  </div>
	   	  	   </figcaption>
	   	  	 </figure>
	   	  </div>
	   	  
-->
	   	  <div class="clearfix"> </div>
	   	 </div>
<!--
	   	 <div class="faculty_top">
	   	  <div class="col-md-4 faculty_grid">
	   	  	<figure class="team_member">
	   	  		<img src="images/fc3.jpg" class="img-responsive wp-post-image" alt=""/>
	   	  		<div></div>
	   	  		<figcaption><h3 class="person-title"><a href="event_single.html">Readable Content </a></h3>
	   	  			<span class="person-deg">Pg, Degree</span>
	   	  			<p><a href="mailto@example.com">info(at)Learn.com</a></p>
	   	  			<p>4 Years</p>
	   	  			<div class="person-social">
	   	  				<ul class="social-person">
	   	  					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-youtube"></i></a></li>
	   	  			    </ul>
	   	  		  </div>
	   	  	   </figcaption>
	   	  	 </figure>
	   	  </div>
	   	  <div class="col-md-4 faculty_grid">
	   	  	<figure class="team_member">
	   	  		<img src="images/fc2.jpg" class="img-responsive wp-post-image" alt=""/>
	   	  		<div></div>
	   	  		<figcaption><h3 class="person-title"><a href="event_single.html">Readable Content </a></h3>
	   	  			<span class="person-deg">Pg, Degree</span>
	   	  			<p><a href="mailto@example.com">info(at)Learn.com</a></p>
	   	  			<p>4 Years</p>
	   	  			<div class="person-social">
	   	  				<ul class="social-person">
	   	  					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-youtube"></i></a></li>
	   	  			    </ul>
	   	  		  </div>
	   	  	   </figcaption>
	   	  	 </figure>
	   	  </div>
	   	  <div class="col-md-4 faculty_grid">
	   	  	<figure class="team_member">
	   	  		<img src="images/fc5.jpg" class="img-responsive wp-post-image" alt=""/>
	   	  		<div></div>
	   	  		<figcaption><h3 class="person-title"><a href="event_single.html">Readable Content </a></h3>
	   	  			<span class="person-deg">Pg, Degree</span>
	   	  			<p><a href="mailto@example.com">info(at)Learn.com</a></p>
	   	  			<p>4 Years</p>
	   	  			<div class="person-social">
	   	  				<ul class="social-person">
	   	  					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-youtube"></i></a></li>
	   	  			    </ul>
	   	  		  </div>
	   	  	   </figcaption>
	   	  	 </figure>
	   	  </div>
	   	  
	   	  <div class="clearfix"> </div>
	   	 </div>
	   	 
-->
<!--
	   	 <div class="faculty_bottom">
	   	  <div class="col-md-4 faculty_grid">
	   	  	<figure class="team_member">
	   	  		<img src="images/fc6.jpg" class="img-responsive wp-post-image" alt=""/>
	   	  		<div></div>
	   	  		<figcaption><h3 class="person-title"><a href="event_single.html">Readable Content </a></h3>
	   	  			<span class="person-deg">Pg, Degree</span>
	   	  			<p><a href="mailto@example.com">info(at)Learn.com</a></p>
	   	  			<p>4 Years</p>
	   	  			<div class="person-social">
	   	  				<ul class="social-person">
	   	  					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-youtube"></i></a></li>
	   	  			    </ul>
	   	  		  </div>
	   	  	   </figcaption>
	   	  	 </figure>
	   	  </div>
	   	  <div class="col-md-4 faculty_grid">
	   	  	<figure class="team_member">
	   	  		<img src="images/fc7.jpg" class="img-responsive wp-post-image" alt=""/>
	   	  		<div></div>
	   	  		<figcaption><h3 class="person-title"><a href="event_single.html">Readable Content </a></h3>
	   	  			<span class="person-deg">Pg, Degree</span>
	   	  			<p><a href="mailto@example.com">info(at)Learn.com</a></p>
	   	  			<p>4 Years</p>
	   	  			<div class="person-social">
	   	  				<ul class="social-person">
	   	  					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-youtube"></i></a></li>
	   	  			    </ul>
	   	  		  </div>
	   	  	   </figcaption>
	   	  	 </figure>
	   	  </div>
	   	  <div class="col-md-4 faculty_grid">
	   	  	<figure class="team_member">
	   	  		<img src="images/fc8.jpg" class="img-responsive wp-post-image" alt=""/>
	   	  		<div></div>
	   	  		<figcaption><h3 class="person-title"><a href="event_single.html">Readable Content </a></h3>
	   	  			<span class="person-deg">Pg, Degree</span>
	   	  			<p><a href="mailto@example.com">info(at)Learn.com</a></p>
	   	  			<p>4 Years</p>
	   	  			<div class="person-social">
	   	  				<ul class="social-person">
	   	  					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
	   	  					<li><a href="#"><i class="fa fa-youtube"></i></a></li>
	   	  			    </ul>
	   	  		  </div>
	   	  	   </figcaption>
	   	  	 </figure>
	   	  </div>
	   	  <div class="clearfix"> </div>
	   	 </div>
	   	 
	     <ul class="pagination">
	   	 	<li class="active"><a href="#">1</a></li>
	   	 	<li><a href="#">2</a></li>
	   	 </ul>
-->
	  </div>
	</div>
    <!--Footer Start--> 
     <?php 
      // this calls the common footer for all the menu pages.
      include_once('footer.php'); 
     ?>
      <!--footer End--> 