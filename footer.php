 <div class="footer">
    	<div class="container">
    		<div class="col-md-3 grid_4">
    		   <h3>About Us</h3>	
    		   <p>Govt. Industrial Training Institute Bharmour</b>, District Chamba was established in the year 2000  with 2 Trades COPA(Computer Operator and Programming Assistant) and Dress Making with one unit each.Two trades viz. Plumber and Surface Ornamentations Technique(Embroidery) one unit of each sanctioned from the Session Aug. 2007 as per Govt. Notification.</p>
    		      <ul class="social-nav icons_2 clearfix">
                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" class="facebook"> <i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a></li>
                 </ul>
    		</div>
    		<div class="col-md-3 grid_4">
    		   <h3>Quick Links</h3>
    			<ul class="footer_list" >
    			    <li><a href="index.php">Home</a></li>
    				<li><a href="shortcodes.php">Principal's Desk</a></li>
    				
    				<li><a href="faculty.php">Faculty</a></li>
    				<li><a href="alumini.php">Alumini</a></li>
    				<li><a href="galary.php">Photo Gallary</a></li>
    			</ul>
    		</div>
    		<div class="col-md-3 grid_4">
    		   <h3>Contact Us</h3>
    			<address>
                    <strong>Govt Industrial Training Institute</strong>
                    <br>
                    <span>Bharmour</span>
                    <br><br>
                    <span>Distric: Chamba (HP) 176315</span>
                    <br>
                    <abbr>Telephone : </abbr> 01895225487
                    <br>
                    <abbr>Email : </abbr> <a href="bmr.iti@gmail.com">bmr.iti@gmail.com</a>
               </address>
    		</div>
    		<div class="col-md-3 grid_4">
    		   <h3>Working Hours</h3>
    			 <table class="table_working_hours">
		        	<tbody>
		        		<tr class="opened_1">
							<td class="day_label">monday</td>
							<td class="day_value">9:30 am - 6.00 pm</td>
						</tr>
					    <tr class="opened">
							<td class="day_label">tuesday</td>
							<td class="day_value">9:30 am - 6.00 pm</td>
						</tr>
					    <tr class="opened">
							<td class="day_label">wednesday</td>
							<td class="day_value">9:30 am - 6.00 pm</td>
						</tr>
					    <tr class="opened">
							<td class="day_label">thursday</td>
							<td class="day_value">9:30 am - 6.00 pm</td>
						</tr>
					    <tr class="opened">
							<td class="day_label">friday</td>
							<td class="day_value">9:30 am - 6.00 pm</td>
						</tr>
					    <tr class="closed">
							<td class="day_label">saturday</td>
							<td class="day_value">9:30 am - 6:00 pm</td>
						</tr>
					    <tr class="closed">
							<td class="day_label">sunday</td>
							<td class="day_value closed"><span>Closed</span></td>
						</tr>
				    </tbody>
				</table>
            </div>
    		<div class="clearfix"> </div>
    		<div class="copy">
		       <p>Copyright © 2016  . All Rights Reserved  | Designed by <a href="http://www.grootcode.com/" target="_blank"><b>GrootCode</b></a> </p>
	        </div>
    	</div>
    </div></div>
    