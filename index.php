<!--Header Start--> 
     <?php 
      // this calls the common header for all the menu pages.
      include_once('header.php'); 
     ?>
      <!--Header End-->  
<!-- banner -->
	<div class="banner" >
			<!-- banner Slider starts Here -->
					<script src="js/responsiveslides.min.js"></script>
					 <script>
						// You can also use "$(window).load(function() {"
						$(function () {
						  // Slideshow 4
						  $("#slider3").responsiveSlides({
							auto: true,
							pager: true,
							nav: true,
							speed: 500,
							namespace: "callbacks",
							before: function () {
							  $('.events').append("<li>before event fired.</li>");
							},
							after: function () {
							  $('.events').append("<li>after event fired.</li>");
							}
						  });
					
						});
					  </script>
					<!--//End-slider-script -->
					<div  id="top" class="callbacks_container">
						<ul class="rslides" id="slider3">
							

							<li>
								<div class="banner-img">
							

								</div>
							</li>



							<li>
								<div class="banner-img2">
									
								</div>
							</li>
							
							<li>
								<div class="banner-img3">
									
								</div>
							</li>
								
						</ul>
		          </div>
	</div>
	<!-- //banner -->

     <div class="grid_1">
     	<div class="container">
     		<div class="col-md-4">
                <div class="panel panel-default">
<div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>News</b></div>
<div class="panel-body">
<div class="row">
<div class="col-xs-12">
<ul class="demo">

<li class="news-item">
<table cellpadding="4">
<tr>
<td><img src="images/1.png" width="60" class="img-circle" /></td>
<td> News 1<a href="#">Read more...</a></td>
</tr>
</table>
</li>

<li class="news-item">
<table cellpadding="4">
<tr>
<td><img src="images/2.png" width="60" class="img-circle" /></td>
<td> News 2<a href="#">Read more...</a></td>
</tr>
</table>
</li>

<li class="news-item">
<table cellpadding="4">
<tr>
<td><img src="images/3.png" width="60" class="img-circle" /></td>
<td> News 3<a href="#">Read more...</a></td>
</tr>
</table>
</li>



</ul>
</div>
</div>
</div>
<div class="panel-footer"> </div>
</div>
            </div>
            <div class="col-md-8 grid_1_right">
              <h2>ITI BHARMOUR</h2> 
              
              <p>
                  Govt. Industrial Training Institute Bharmour</b>, District Chamba was established in the year 2000  with 2 Trades COPA(Computer Operator and Programming Assistant) and Dress Making with one unit each.Two trades viz. Plumber and Surface Ornamentations Technique(Embroidery) one unit of each sanctioned from the Session Aug. 2007 as per Govt. Notification. <br>
                  
                  Bharmour ITI College is one of the best Govt. Industrial Training Institute and providing best technical and practical education according to job oriented process.
              </p>
		    <br>
		    
		    
		    
		    

		    
		    
      </div>
      
      
      
      <div class="clearfix"> </div>
     

   <div class="bottom_content">  
   	 <h3>Our Affiliation </h3>
     <div class="grid_2">
     	<div class="col-md-4 portfolio-left">
            <div class="portfolio-img event-img">
                <img src="images/7.jpg" class="img-responsive" alt=""/>
                <div class="over-image"></div>
            </div>
            <div class="portfolio-description">
               <h4><a href="#">ADMISSION</a></h4>
               <p>Education is the passport of the future, for tommorow belongs to those who prepare for it today.</p>
                
                <a href="http://hptsb.onlineadmission.net/OLAHPTSB/olaStaticGroupHomePageAction.action" target="_blank">
                    <span><i class="fa fa-chain chain_1"></i>Online Admission</span>
                </a>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="col-md-4 portfolio-left">
            <div class="portfolio-img event-img">
                <img src="images/dgt.png" class="img-responsive" alt=""/>
                 <div class="over-image"></div>
            </div>
            <div class="portfolio-description">
               <h4><a href="#">DGT</a></h4>
               <p>Ministry of skill development and entrepreneurship <br> (Government of INDIA)</p>
               
                <a href="http://www.dget.nic.in/" target="_blank">
                    <span><i class="fa fa-chain chain_1"></i>Official Website</span>
                </a>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="col-md-4 portfolio-left">
            <div class="portfolio-img event-img">
                <img src="images/hpts.png" class="img-responsive" alt=""/>
                 <div class="over-image"></div>
            </div>
            <div class="portfolio-description">
               <h4><a href="#">HPTSB</a></h4>
               <p>Himachal Pradesh Takniki Shiksha Board Dharamshala <br>Himachal Pradesh</p>
               
                <a href="http://www.hptechboard.com/" target="_blank">
                    <span><i class="fa fa-chain chain_1"></i>Official Website</span>
                </a>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="clearfix"> </div>
     </div>
     
    

    <!--Footer Start--> 
     <?php 
      // this calls the common footer for all the menu pages.
      include_once('footer.php'); 
     ?>
      <!--footer End--> 
   
<script src="js/jquery.countdown.js"></script>
<script src="js/script.js"></script>
<script type="text/javascript">
$(function () {
$(".demo").bootstrapNews({
newsPerPage: 4,
navigation: true,
autoplay: true,
direction:'up', // up or down
animationSpeed: 'normal',
newsTickerInterval: 4000, //4 secs
pauseOnHover: true,
onStop: null,
onPause: null,
onReset: null,
onPrev: null,
onNext: null,
onToDo: null
});
});
</script>
