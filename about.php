<!--Header Start--> 
     <?php 
      // this calls the common header for all the menu pages.
      include_once('header.php'); 
     ?>
      <!--Header End--> 
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>About</h3>
  		
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="index.html">Home</a></li>
                <li class="current-page">About</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="courses_box1">
	   <div class="container">
	   	  <div class="col-md-6 about_left">
	   	  	<h1>ABOUT ITI BHARMOUR</h1>
	   	  	<p><b>Govt. Industrial Training Institute Bharmour</b>, District Chamba was established in the year 2000  with 2 Trades COPA(Computer Operator and Programming Assistant) and Dress Making with one unit each.Two trades viz. Plumber and Surface Ornamentations Technique(Embroidery) one unit of each sanctioned from the Session Aug. 2007 as per Govt. Notification. </p>
	   	  	
	   	  	<h3>Affiliation Status</h3>
	   	  	<p>
	   	  	    All the Four trades namely COPA, Dress Making, Plumber and Embroidery are  running NCVT.  The new affiliation file has yet to be submitted due to Space, Machinery & Equipment not available as per NCVT Norms and the process for this is going on. 
	   	  	</p>
	   	    
           <h3>Land</h3>
	   	  	<p>
	   	  	    Land measuring 04-1-00 Bighas has been transferred in the name of Govt. Industrial Training Institute Bharmour, with some old building constructed by Forest Department and there is sufficient space available with the department for the constructions of new building ITI Bharmour.
                The workshop and other space required is not available in the present building allotted to the institute. The construction of new building has  not yet started. Other than the above as mentioned in the table there is no other space available at present in the institutional building

	   	  	</p>
           
           <h3>Building</h3>
	   	  	<p>
	   	  	   Presently ITI Bharmour is running in the old building transferred from the forest department. The Land measuring 04-1-00 Bighas has been transferred in the name of Department for constructions of new ITI building. The funds amounting to INR 67,50,000.00 has been deposited to the Executive Engineer HPPWD Bharmour division. Request has been made to HPPWD Bharmour to start the construction work of new  ITI building .
	   	  	</p>
	   	    
           <h3>Introduction of new trades</h3>
	   	  	<p>
	   	  	    At present there is no possibility to start new trade due to the shortage of infrastructure and damaged road approach.
	   	  	</p>
            
          </div>
	   	  <div class="col-md-6">
	   	  	<img src="images/8.jpg" class="img-responsive" alt="" /><br>
	   	  	<img src="images/event2.jpg" class="img-responsive" alt=""/> <br>
	       <img src="images/6.jpg" class="img-responsive" alt=""/> <br>
	       <img src="images/9.jpg" class="img-responsive" alt=""/> <br>


	   	  	
	   	  	
	   	  </div>
	   	  <div class="clearfix"> </div>
	   </div>
	</div>

   <!--Footer Start--> 
     <?php 
      // this calls the common footer for all the menu pages.
      include_once('footer.php'); 
     ?>
      <!--footer End--> 