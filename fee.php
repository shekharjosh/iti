 <!--Header Start--> 
     <?php 
      // this calls the common header for all the menu pages.
      include_once('header.php'); 
     ?>
      <!--Header End--> 
    
    <!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Fee</h3>
  		
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="index.html">Home</a></li>
                <li class="current-page">Fee</li>
            </ul>
        </div>
  	</div>
  </div>
    
    <div class="features">
	   <div class="container">
	   	  <h2>FEE STRUCTURE</h2>

            <table class="responstable">
  
          <tr>
            <th>Serial No.</th>
            <th> Description</th>
            <th>Amount (INR)</th>
            
            
          </tr>
          <tr>
    
    <td>1</td>
    <td>Admission Fee (One Time)</td>
    <td>200</td>
    
  </tr>
  
  <tr>
    
    <td>2</td>
    <td>Institutional Security (One Time)</td>
    <td>1000</td>

  </tr>
  
  <tr>
    <td>3</td>
    <td>Identity Card Fund (1 Year)</td>
    <td>50</td>
    
  </tr>
  
  <tr>
    <td>4</td>
    <td>Student Welfare Fund (Every Year)</td>
    <td>350</td>
    
  </tr>
  
  <tr>
    <td>5</td>
    <td>Building Fund (Per Semester) </td>
    <td>250</td>
    
  </tr>
  
  <tr>
    <td>6</td>
    <td>Medical Fund (Every Year)</td>
    <td>100</td>
    
  </tr>
  
   <tr>
    <td>7</td>
    <td>Examination Fund</td>
    <td>100</td>
    
  </tr>
  
  <tr>
    <td>8</td>
    <td>Tution Fee</td>
    <td>1200</td>
    
  </tr>
  
  <tr>
    <td>9</td>
    <td>Electricity & Water Charges</td>
    <td>250</td>
    
  </tr>
  
   <tr>
    <td>10</td>
    <td>Computer Fee</td>
    <td>600</td>
   
  </tr>
  
   <tr>
    <td>11</td>
    <td>N.C.C.</td>
    <td>20</td>
    
  </tr>
  
  <tr>
    <td>12</td>
    <td>Library Charges</td>
    <td>100</td>
    
  </tr>
  
  <tr>
    
   <td>13</td>
    <td>Training & Placement Fund (One Time)</td>
    <td>100</td>
   
  </tr>
  
  <tr>
    <td>14</td>
    <td>Insurance LIC (Every Year)</td>
    <td>200</td>
   
  </tr>
  
   <tr>
    <td>15</td>
    <td>RNTC & NTC</td>
    <td>100</td>
    
  </tr> 
  
  <tr>
    <td></td>
    <td>Total</td>
    <td>4620</td>
    
  </tr> 
</table>
    <script src='js/respond.js'></script>
    
    
    
    
    <!--Footer Start--> 
     <?php 
      // this calls the common footer for all the menu pages.
      include_once('footer.php'); 
     ?>
      <!--footer End--> 