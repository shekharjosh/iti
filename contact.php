<!--Header Start--> 
     <?php 
      // this calls the common header for all the menu pages.
      include_once('header.php'); 
     ?>
      <!--Header End--> 
<!-- banner -->
  <div class="courses_banner">
  	<div class="container">
  		<h3>Contact</h3>
  		
        <div class="breadcrumb1">
            <ul>
                <li class="icon6"><a href="index.html">Home</a></li>
                <li class="current-page">Contact</li>
            </ul>
        </div>
  	</div>
  </div>
    <!-- //banner -->
	<div class="features">
	   <div class="container">
	   	  <h1>How to find us</h1>
	   	  <div class="map">
			 <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3150859.767904157!2d-96.62081048651531!3d39.536794757966845!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1408111832978"> </iframe>
		  </div>
		  
		  <div class="wrapper">
				<div class="col_1">
					<i class="fa fa-home  icon2"></i>
					<div class="box">
						<p class="marTop9">7401 Excepteur sint,<br>Deserunt mollit .</p>
					</div>
				</div>

				<div class="col_2">
					<i class="fa fa-phone  icon2"></i>
					<div class="box">
						<p class="marTop9">+1 800 254 5478<br>+1 800 587 47895</p>
					</div>
				</div>

				<div class="col_2">
					<i class="fa fa-envelope icon2"></i>
					<div class="box">
						<p class="m_6"><a href="mailto@example.com" class="link4">info(at)Learn.com</a></p>
					</div>
				</div>
				<div class="clearfix"> </div>
		 </div>
		 <form class="contact_form">
		 	<h2>Contact form</h2>
			<div class="col-md-6 grid_6">
				<input type="text" class="text" value="Name" placeholder="name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}">
				<input type="text" class="text" value="Email" placeholder="email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
				<input type="text" class="text" value="Phone" placeholder="phone" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Phone';}">
			</div>
	
			<div class="col-md-6 grid_6">
				<textarea value="Message" placeholder="message" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>
			</div>
            <div class="clearfix"> </div>
            <div class="btn_3">
			  <a href="#" class="more_btn" data-type="submit">Send message</a>
		    </div>
		 </form>
							
						
	  </div>
	</div>
	
	<!--Footer Start--> 
     <?php 
      // this calls the common footer for all the menu pages.
      include_once('footer.php'); 
     ?>
      <!--footer End--> 