<!--Header Start--> 
     <?php 
      // this calls the common header for all the menu pages.
      include_once('header.php'); 
     ?>
      <!--Header End--> 
      
 <div class="container" style="background: url('images/t2.jpg'); background-repeat: no-repeat; background-size: cover; padding:0;">
 <div  style="background-color: rgba(45,66,120,.4); padding:0 15px;">   
  
    <br>
    <!-- The container for the list of example images -->
    <div id="links" class="cs">
    <a href="images/1.jpg" data-gallery>
        <img src="images/1.jpg" alt="Banana">
    </a>
    <a href="images/2.jpg"  data-gallery>
        <img src="images/2.jpg" alt="Banana">
    </a>
    <a href="images/3.jpg"  data-gallery>
        <img src="images/3.jpg" alt="Banana">
          </a>
  
    <a href="images/4.jpg"  data-gallery>
        <img src="images/4.jpg" alt="Banana">    
    </a>
    
    <a href="images/event.jpg"  data-gallery>
        <img src="images/event.jpg" alt="Banana">
    </a>
    
    <a href="images/6.jpg"  data-gallery>
        <img src="images/6.jpg" alt="Banana">
    </a>
    
    <a href="images/8.jpg"  data-gallery>
        <img src="images/8.jpg" alt="Banana">
    </a>
    
    <a href="images/9.jpg"  data-gallery>
        <img src="images/9.jpg" alt="Banana">
    </a>
    
   
    
    
</div>
    <br>
</div>
</div>
<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery" >
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
  
  <!--Footer Start--> 
     <?php 
      // this calls the common footer for all the menu pages.
      include_once('footer.php'); 
     ?>
      <!--footer End--> 
      
     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation and button states -->
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<script src="js/bootstrap-image-gallery.js"></script>
<script src="js/demo.js"></script>